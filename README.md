Pandora-QT

Pandora-QT is a linux Pandora Radio client for Linux. It fully supports all the webapp allows such as modifying stations, playlists, and settings. It is larger due to it using PyQT(PySide2) Qtwebengine Wrapper.

License: GNU GPLv3

Pandora-QT is not affiliated with or endorsed by Pandora Media, Inc.

##Features

1.Resizable.

2.Artists, Albums, Songs, Stations, Playlists, Podcasts, Episodes and collection support.

3.OpenSource. Thanks to QT for Python PySide2

Special Shoutout to: https://www.learnpyqt.com/ Learned alot from this site.

https://github.com/mfitzp

The Webapp is also based this on a very minimal version of Mooseache
 https://github.com/learnpyqt/15-minute-apps/tree/master/browser


##How to use
To download and use the app. Download both the executable and the PandoraQt.desktop shortcut launcher to the same folder or desktop

Download the Executable:
wget https://gitlab.com/mikeramsey/pandora-qt/-/blob/master/dist/PandoraQt

Download the Desktop launcher:
wget https://gitlab.com/mikeramsey/pandora-qt/-/raw/master/PandoraQt.desktop?inline=false

Right click and allow the permissions for your user to execute the file.

Double click to open and login to the app.

Enjoy full functionality of Pandora Radio in a resizable native app.

##Screenshots

![alt text](Screenshots/Pandora-QT_860.png "PandoraQT Screenshot")

![alt text](Screenshots/Pandora-QT_854.png "PandoraQT Screenshot")

![alt text](Screenshots/Pandora-QT_855.png "PandoraQT Screenshot")

![alt text](Screenshots/Pandora-QT_856.png "PandoraQT Screenshot")

![alt text](Screenshots/Pandora-QT_857.png "PandoraQT Screenshot")

![alt text](Screenshots/Pandora-QT_858.png "PandoraQT Screenshot")

![alt text](Screenshots/Pandora-QT_859.png "PandoraQT Screenshot")

