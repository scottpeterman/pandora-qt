# from PyQt5.QtCore import *
# from PyQt5.QtWidgets import *
# from PyQt5.QtGui import *
# from PyQt5.QtWebEngineWidgets import *
# from PyQt5.QtPrintSupport import *

from PySide2.QtCore import QUrl
from PySide2.QtGui import QIcon
from PySide2.QtWebEngineWidgets import QWebEngineView
from PySide2.QtWidgets import QApplication, QMainWindow, QStatusBar

import os
import sys



class MainWindow(QMainWindow):
    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)

        self.browser = QWebEngineView()
        self.browser.setUrl(QUrl("https://www.pandora.com/"))
        self.setCentralWidget(self.browser)

        self.status = QStatusBar()
        self.setStatusBar(self.status)

        # Uncomment to disable native menubar on Mac
        # self.menuBar().setNativeMenuBar(False)


        self.show()

        self.setWindowIcon(QIcon(os.path.join('images', 'icons8-radio-64.png')))


app = QApplication(sys.argv)
app.setApplicationName("Pandora-QT")
app.setOrganizationName("Pandora-QT")
app.setOrganizationDomain("Pandora-QT.org")

window = MainWindow()

app.exec_()
